﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLib
{
    public class FishSource
    {
        public static List<FishDTO> Fishes()
        {
            var result = new List<FishDTO>();
            result.Add(new FishDTO("Koho"
                , @"
                    The coho salmon is a species of anadromous fish in the salmon family,
                    one of the several species of Pacific salmon. Coho salmon are also known as silver salmon or silvers.
                    The scientific species name is based on the Russian common name kizhuch.
                    "
                ,"koho"));

            result.Add(new FishDTO("rainbow"
               , @"
                     (Oncorhynchus mykiss) is a trout and species of salmonid native to cold-water tributaries of the Pacific Ocean in Asia and North America. 
                     The steelhead (sometimes called steelhead trout) is an anadromous (sea-run) form of the coastal rainbow trout (O. m. irideus) or Columbia River redband trout 
                     (O. m. gairdneri) that usually returns to fresh water to spawn after living two to three years in the ocean.
                     Freshwater forms that have been introduced into the Great Lakes and migrate into tributaries to spawn are also called steelhead.
                    "
               , "rainbow"));

            result.Add(new FishDTO("char"
               , @"
                    The coho salmon is a species of anadromous fish in the salmon family,
                    one of the several species of Pacific salmon. Coho salmon are also known as silver salmon or silvers.
                    The scientific species name is based on the Russian common name kizhuch.
                    "
               , "Chasssr"));


            return result;
        }
    }
}
