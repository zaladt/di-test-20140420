﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLib
{
    public class FishDTO
    {
        public FishDTO(string name,string description,string imagekey)
        {
            Name = name;
            Description = description;
            ImageKey = imagekey;
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ImageKey { get; set; }

    }
}
