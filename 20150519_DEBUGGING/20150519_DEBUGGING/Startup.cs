﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_20150519_DEBUGGING.Startup))]
namespace _20150519_DEBUGGING
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
