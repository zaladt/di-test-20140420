﻿using System.Web;
using System.Web.Mvc;

namespace _20150519_DEBUGGING
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
