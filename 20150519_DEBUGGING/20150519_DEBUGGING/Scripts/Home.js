﻿function DisplayFish(value) {

    if (value === 'All') {
        $.each($('.fishcontainer'), function (i, item) {
            $(item).fadeIn();
        })
    }
    else {

        $.each($('.fishcontainer'), function (i, item) {

            var id = $(item).attr('id');

            if (id !== value) {
                $(item).fadeOut();
            } else {
                 $(item).fadeIn();
            }

        })
    };
}