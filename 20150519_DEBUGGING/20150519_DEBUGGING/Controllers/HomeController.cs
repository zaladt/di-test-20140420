﻿using _20150519_DEBUGGING.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _20150519_DEBUGGING.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
           var fishies = SharedLib.FishHandler.GetFishes();    
           return View(new HomeModel() { Fishes = fishies });
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}