﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using _20150519_DEBUGGING.Models;
using System.IO;
using System.Xml;
using System.Text;
using System.IO;

namespace _20150519_DEBUGGING.Controllers
{
    public class PoorlyConstructedCodeController : Controller
    {
        // GET: PoorlyConstructedCode
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Send()
        {
            logItem("sada","asdsad","asdasd","asdasd","asdadsd","asdsasd");
            return Content("Email sent!");
        }

        //Call this to log the email a friend form to XML file.
        private void logItem(string strURL, string strFriendName, string strFriendEmail, string strYourName,
            string strYourEmail, string strMessage)
        {
            string strTime = String.Format("{0:HH:mm:ss}", DateTime.Now);
            string strDate = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

            string filePath = Server.MapPath("~/bin/log.xml");

            
            //Check if XML file exists
            if (!System.IO.File.Exists(filePath))
            {
                //XML file DOES not exist yet.

                // Create a new XmlTextWriter instance
                XmlTextWriter writer = new XmlTextWriter(filePath, Encoding.UTF8);

                //Set the XML writer to indent the XML as opposed to all one line.
                writer.Formatting = Formatting.Indented;

                // start writing XML!
                writer.WriteStartDocument();

                // Create the <logs> root XML node
                writer.WriteStartElement("logs");

                // Creating the <log> node
                writer.WriteStartElement("log");

                // Adding the time attribute to the <log> node. <log time="14:52:23">
                writer.WriteAttributeString("time", strTime);

                // Adding the date attribute to the <log> node. <log time="14:52:23" date="22/02/2009">
                writer.WriteAttributeString("date", strDate);


                /*
                    ========================================================
                    Add the <field alias="url"> node
                    ========================================================
                    */
                // Creating the <field> node inside the <log> node
                writer.WriteStartElement("field");

                //Add the alias attribute to the <field> node. <field alias="url">
                writer.WriteAttributeString("alias", "url");

                //Write value of the field node.
                writer.WriteString(strURL);

                //Close the <field> node inside the <log> node
                writer.WriteEndElement();


                /*
                    ========================================================
                    Add the <field alias="friendName"> node
                    ========================================================
                    */

                // Creating the <field> node inside the <log> node
                writer.WriteStartElement("field");

                //Add the alias attribute to the <field> node. <field alias="friendName">
                writer.WriteAttributeString("alias", "friendName");

                //Write value of the field node.
                writer.WriteString(strFriendName);

                //Close the <field> node inside the <log> node
                writer.WriteEndElement();


                /*
                    ========================================================
                    Add the <field alias="friendEmail"> node
                    ========================================================
                    */

                // Creating the <field> node inside the <log> node
                writer.WriteStartElement("field");

                //Add the alias attribute to the <field> node. <field alias="friendEmail">
                writer.WriteAttributeString("alias", "friendEmail");

                //Write value of the field node.
                writer.WriteString(strFriendEmail);

                //Close the <field> node inside the <log> node
                writer.WriteEndElement();


                /*
                    ========================================================
                    Add the <field alias="yourName"> node
                    ========================================================
                    */

                // Creating the <field> node inside the <log> node
                writer.WriteStartElement("field");

                //Add the alias attribute to the <field> node. <field alias="yourName">
                writer.WriteAttributeString("alias", "yourName");

                //Write value of the field node.
                writer.WriteString(strYourName);

                //Close the <field> node inside the <log> node
                writer.WriteEndElement();


                /*
                    ========================================================
                    Add the <field alias="yourEmail"> node
                    ========================================================
                    */

                // Creating the <field> node inside the <log> node
                writer.WriteStartElement("field");

                //Add the alias attribute to the <field> node. <field alias="yourEmail">
                writer.WriteAttributeString("alias", "yourEmail");

                //Write value of the field node.
                writer.WriteString(strYourEmail);

                //Close the <field> node inside the <log> node
                writer.WriteEndElement();


                /*
                    ========================================================
                    Add the <field alias="message"> node
                    ========================================================
                    */
                // Creating the <field> node inside the <log> node
                writer.WriteStartElement("field");

                //Add the alias attribute to the <field> node. <field alias="message">
                writer.WriteAttributeString("alias", "message");

                //Write value of the field node, wrapped inside a CData tag
                writer.WriteCData(strMessage);

                //Close the <field> node inside the <log> node
                writer.WriteEndElement();



                //Close the <log> node
                writer.WriteEndElement();

                //Close the <logs> node
                writer.WriteEndDocument();

                //Stop using the XML writer & close the XML file.
                writer.Close();

            }
            else
            {
                //XML file already exists so lets add to the XML file.

                //Open up the XML file we already have.
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(filePath);


                //Create a new XML <log> node
                XmlElement logNode = xmlDoc.CreateElement("log");

                // Adding the time attribute to the <log> node. <log time="14:52:23">
                logNode.SetAttribute("time", strTime);

                // Adding the date attribute to the <log> node. <log time="14:52:23" date="22/02/2009">
                logNode.SetAttribute("date", strDate);


                /*
                    ========================================================
                    Add the <field alias="url"> node
                    ========================================================
                    */

                //Create a new xml <field> node
                XmlElement fieldNodeURL = xmlDoc.CreateElement("field");

                //Add the alias attribute to the <field> node. <field alias="url">
                fieldNodeURL.SetAttribute("alias", "url");

                //Write value of the field node.
                fieldNodeURL.InnerText = strURL;

                //Add our <field> node to our <log> node
                logNode.AppendChild(fieldNodeURL);

                /*
                    ========================================================
                    Add the <field alias="friendName"> node
                    ========================================================
                    */

                //Create a new xml <field> node
                XmlElement fieldNodeFriendName = xmlDoc.CreateElement("field");

                //Add the alias attribute to the <field> node. <field alias="friendName">
                fieldNodeFriendName.SetAttribute("alias", "friendName");

                //Write value of the field node.
                fieldNodeFriendName.InnerText = strFriendName;

                //Add our <field> node to our <log> node
                logNode.AppendChild(fieldNodeFriendName);


                /*
                    ========================================================
                    Add the <field alias="friendEmail"> node
                    ========================================================
                    */

                //Create a new xml <field> node
                XmlElement fieldNodeFriendEmail = xmlDoc.CreateElement("field");

                //Add the alias attribute to the <field> node. <field alias="friendEmail">
                fieldNodeFriendEmail.SetAttribute("alias", "friendEmail");

                //Write value of the field node.
                fieldNodeFriendEmail.InnerText = strFriendEmail;

                //Add our <field> node to our <log> node
                logNode.AppendChild(fieldNodeFriendEmail);


                /*
                    ========================================================
                    Add the <field alias="yourName"> node
                    ========================================================
                    */

                //Create a new xml <field> node
                XmlElement fieldNodeYourName = xmlDoc.CreateElement("field");

                //Add the alias attribute to the <field> node. <field alias="yourName">
                fieldNodeYourName.SetAttribute("alias", "yourName");

                //Write value of the field node.
                fieldNodeYourName.InnerText = strYourName;

                //Add our <field> node to our <log> node
                logNode.AppendChild(fieldNodeYourName);

                /*
                    ========================================================
                    Add the <field alias="yourEmail"> node
                    ========================================================
                    */

                //Create a new xml <field> node
                XmlElement fieldNodeYourEmail = xmlDoc.CreateElement("field");

                //Add the alias attribute to the <field> node. <field alias="yourEmail">
                fieldNodeYourEmail.SetAttribute("alias", "yourEmail");

                //Write value of the field node.
                fieldNodeYourEmail.InnerText = strYourEmail;

                //Add our <field> node to our <log> node
                logNode.AppendChild(fieldNodeYourEmail);


                /*
                    ========================================================
                    Add the <field alias="message"> node
                    ========================================================
                    */

                //Create a new xml <field> node
                XmlElement fieldNodeMessage = xmlDoc.CreateElement("field");

                //Add the alias attribute to the <field> node. <field alias="message">
                fieldNodeMessage.SetAttribute("alias", "message");

                //Create a CDATA section that wraps the message text
                XmlCDataSection fieldNodeCData = xmlDoc.CreateCDataSection(strMessage);

                //Add our cdata tag/node to our <field> node
                fieldNodeMessage.AppendChild(fieldNodeCData);

                //Add our <field> node to our <log> node
                logNode.AppendChild(fieldNodeMessage);


                //Add our built up logNode that contains our <field> nodes to the main xml document
                xmlDoc.SelectSingleNode("/logs").AppendChild(logNode);


                //Save the file
                xmlDoc.Save(filePath);
            }
        }
    }
}