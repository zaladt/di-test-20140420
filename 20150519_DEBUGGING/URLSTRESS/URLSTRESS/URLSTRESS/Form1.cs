﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace URLSTRESS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        static bool _shouldStop = false;
        static int numWorkItems = 0;
        static int requests = 0;
        static int requestsLastSec = 0;
        static int sec = 0;
        static int count200 = 0;
        static int count401 = 0;
        static int count404 = 0;
        static int count304 = 0;
        static int countFailures = 0;


        public void WorkerThreadProc()
        {
            Interlocked.Increment(ref numWorkItems);
            int tries = 0;
            int MaxTries = Int32.Parse(TextRepeatCount.Text);
            while ((!_shouldStop) && ((tries < Int32.Parse(TextRepeatCount.Text)||(CBforever.Checked))))               
            {
                // Console.WriteLine("worker thread: working...");
                // Create a request using a URL that can receive a post. 
                HttpWebRequest request = (HttpWebRequest ) WebRequest.Create(TextURL.Text);
                request.KeepAlive=true;                
                // Set the Method property of the request to POST.

                try
                {
                    request.Method = "GET";
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.OK) Interlocked.Increment(ref count200);
                    if (response.StatusCode == HttpStatusCode.Unauthorized) Interlocked.Increment(ref count401);
                    if (response.StatusCode == HttpStatusCode.NotFound) Interlocked.Increment(ref count404);
                    if (response.StatusCode == HttpStatusCode.NotModified) Interlocked.Increment(ref count304);
                    response.Close();
                }
                catch
                {
                     Interlocked.Increment(ref countFailures);
                }

                tries++;
                Interlocked.Increment(ref requests);
                Interlocked.Increment(ref requestsLastSec);
            }
            Interlocked.Decrement(ref numWorkItems);
        }
        
        private void ButtonRun_Click(object sender, EventArgs e)
        {
            ButtonRun.Enabled = false;
            ButtonStop.Enabled = true;
            buttonReset.Enabled = true;
            _shouldStop = false; 
            sec = 0;
            requests = 0;
            count200 = 0;
            count401 = 0;
            count404 = 0;
            count304 = 0;
            countFailures = 0;
            
            // Start the worker threads.
            for (int i = 1; i <= Int32.Parse(TextThreads.Text); i++)
            {
                Thread workerThread = new Thread(WorkerThreadProc);
                workerThread.Start();
            }
            timer.Start();

        }

        private void ButtonStop_Click(object sender, EventArgs e)
        {           
            _shouldStop = true;           
            
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            sec = sec + 1;
            labelValueRequests.Text=requests.ToString();
            labelValueTime.Text = sec.ToString();
            labelRequestsPerSec.Text = Convert.ToDecimal(requests / sec).ToString();
            labelCount200.Text = count200.ToString();
            /*labelCount401.Text = count401.ToString();
            labelCount304.Text = count304.ToString();
            labelCount404.Text = count404.ToString();*/
            labelCountFailures.Text = countFailures.ToString();

            labelRequestsLastSec.Text = Convert.ToDecimal(requestsLastSec ).ToString();
            requestsLastSec = 0;
            
            if (numWorkItems == 0)
            {
                timer.Stop();
                ButtonRun.Enabled = true;
                buttonReset.Enabled = false;
                ButtonStop.Enabled = false;
                
            }
        }

        private void CBforever_CheckedChanged(object sender, EventArgs e)
        {
            TextRepeatCount.Enabled = !(CBforever.Checked);
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            requests = 0;
            sec = 0;
            count200 = 0;
            countFailures = 0;
        }


    }
}
