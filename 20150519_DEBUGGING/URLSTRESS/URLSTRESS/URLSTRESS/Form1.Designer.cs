﻿namespace URLSTRESS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TextURL = new System.Windows.Forms.TextBox();
            this.labelURL = new System.Windows.Forms.Label();
            this.labelRepeat = new System.Windows.Forms.Label();
            this.ButtonRun = new System.Windows.Forms.Button();
            this.labelRequests = new System.Windows.Forms.Label();
            this.labelTheads = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelStats = new System.Windows.Forms.Label();
            this.labelValueRequests = new System.Windows.Forms.Label();
            this.labelValueTime = new System.Windows.Forms.Label();
            this.labelRequestsPerSec = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.ButtonStop = new System.Windows.Forms.Button();
            this.label200 = new System.Windows.Forms.Label();
            this.labelFailures = new System.Windows.Forms.Label();
            this.labelCount200 = new System.Windows.Forms.Label();
            this.labelCountFailures = new System.Windows.Forms.Label();
            this.TextThreads = new System.Windows.Forms.MaskedTextBox();
            this.TextRepeatCount = new System.Windows.Forms.MaskedTextBox();
            this.CBforever = new System.Windows.Forms.CheckBox();
            this.buttonReset = new System.Windows.Forms.Button();
            this.labelRequestLastSec = new System.Windows.Forms.Label();
            this.labelRequestsLastSec = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TextURL
            // 
            this.TextURL.Location = new System.Drawing.Point(123, 10);
            this.TextURL.Name = "TextURL";
            this.TextURL.Size = new System.Drawing.Size(480, 20);
            this.TextURL.TabIndex = 0;
            this.TextURL.Text = "http://localhost/";
            // 
            // labelURL
            // 
            this.labelURL.AutoSize = true;
            this.labelURL.Location = new System.Drawing.Point(15, 13);
            this.labelURL.Name = "labelURL";
            this.labelURL.Size = new System.Drawing.Size(29, 13);
            this.labelURL.TabIndex = 1;
            this.labelURL.Text = "URL";
            // 
            // labelRepeat
            // 
            this.labelRepeat.AutoSize = true;
            this.labelRepeat.Location = new System.Drawing.Point(15, 60);
            this.labelRepeat.Name = "labelRepeat";
            this.labelRepeat.Size = new System.Drawing.Size(72, 13);
            this.labelRepeat.TabIndex = 2;
            this.labelRepeat.Text = "Repeat count";
            // 
            // ButtonRun
            // 
            this.ButtonRun.Location = new System.Drawing.Point(278, 111);
            this.ButtonRun.Name = "ButtonRun";
            this.ButtonRun.Size = new System.Drawing.Size(73, 30);
            this.ButtonRun.TabIndex = 4;
            this.ButtonRun.Text = "Run";
            this.ButtonRun.UseVisualStyleBackColor = true;
            this.ButtonRun.Click += new System.EventHandler(this.ButtonRun_Click);
            // 
            // labelRequests
            // 
            this.labelRequests.AutoSize = true;
            this.labelRequests.Location = new System.Drawing.Point(15, 120);
            this.labelRequests.Name = "labelRequests";
            this.labelRequests.Size = new System.Drawing.Size(58, 13);
            this.labelRequests.TabIndex = 5;
            this.labelRequests.Text = "Request(s)";
            // 
            // labelTheads
            // 
            this.labelTheads.AutoSize = true;
            this.labelTheads.Location = new System.Drawing.Point(15, 35);
            this.labelTheads.Name = "labelTheads";
            this.labelTheads.Size = new System.Drawing.Size(46, 13);
            this.labelTheads.TabIndex = 7;
            this.labelTheads.Text = "Threads";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(15, 97);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(76, 13);
            this.labelTime.TabIndex = 9;
            this.labelTime.Text = "Time from start";
            // 
            // labelStats
            // 
            this.labelStats.AutoSize = true;
            this.labelStats.Location = new System.Drawing.Point(15, 141);
            this.labelStats.Name = "labelStats";
            this.labelStats.Size = new System.Drawing.Size(98, 13);
            this.labelStats.TabIndex = 10;
            this.labelStats.Text = "Requests / second";
            // 
            // labelValueRequests
            // 
            this.labelValueRequests.AutoSize = true;
            this.labelValueRequests.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValueRequests.Location = new System.Drawing.Point(118, 120);
            this.labelValueRequests.Name = "labelValueRequests";
            this.labelValueRequests.Size = new System.Drawing.Size(13, 13);
            this.labelValueRequests.TabIndex = 11;
            this.labelValueRequests.Text = "0";
            // 
            // labelValueTime
            // 
            this.labelValueTime.AutoSize = true;
            this.labelValueTime.Location = new System.Drawing.Point(120, 97);
            this.labelValueTime.Name = "labelValueTime";
            this.labelValueTime.Size = new System.Drawing.Size(13, 13);
            this.labelValueTime.TabIndex = 12;
            this.labelValueTime.Text = "0";
            // 
            // labelRequestsPerSec
            // 
            this.labelRequestsPerSec.AutoSize = true;
            this.labelRequestsPerSec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelRequestsPerSec.Location = new System.Drawing.Point(119, 141);
            this.labelRequestsPerSec.Name = "labelRequestsPerSec";
            this.labelRequestsPerSec.Size = new System.Drawing.Size(13, 13);
            this.labelRequestsPerSec.TabIndex = 13;
            this.labelRequestsPerSec.Text = "0";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // ButtonStop
            // 
            this.ButtonStop.Enabled = false;
            this.ButtonStop.Location = new System.Drawing.Point(357, 111);
            this.ButtonStop.Name = "ButtonStop";
            this.ButtonStop.Size = new System.Drawing.Size(73, 30);
            this.ButtonStop.TabIndex = 14;
            this.ButtonStop.Text = "Stop";
            this.ButtonStop.UseVisualStyleBackColor = true;
            this.ButtonStop.Click += new System.EventHandler(this.ButtonStop_Click);
            // 
            // label200
            // 
            this.label200.AutoSize = true;
            this.label200.Location = new System.Drawing.Point(15, 183);
            this.label200.Name = "label200";
            this.label200.Size = new System.Drawing.Size(58, 13);
            this.label200.TabIndex = 15;
            this.label200.Text = "Status 200";
            // 
            // labelFailures
            // 
            this.labelFailures.AutoSize = true;
            this.labelFailures.Location = new System.Drawing.Point(15, 206);
            this.labelFailures.Name = "labelFailures";
            this.labelFailures.Size = new System.Drawing.Size(43, 13);
            this.labelFailures.TabIndex = 19;
            this.labelFailures.Text = "Failures";
            // 
            // labelCount200
            // 
            this.labelCount200.AutoSize = true;
            this.labelCount200.Location = new System.Drawing.Point(120, 183);
            this.labelCount200.Name = "labelCount200";
            this.labelCount200.Size = new System.Drawing.Size(13, 13);
            this.labelCount200.TabIndex = 20;
            this.labelCount200.Text = "0";
            // 
            // labelCountFailures
            // 
            this.labelCountFailures.AutoSize = true;
            this.labelCountFailures.Location = new System.Drawing.Point(120, 206);
            this.labelCountFailures.Name = "labelCountFailures";
            this.labelCountFailures.Size = new System.Drawing.Size(13, 13);
            this.labelCountFailures.TabIndex = 24;
            this.labelCountFailures.Text = "0";
            // 
            // TextThreads
            // 
            this.TextThreads.Location = new System.Drawing.Point(123, 32);
            this.TextThreads.Mask = "999";
            this.TextThreads.Name = "TextThreads";
            this.TextThreads.Size = new System.Drawing.Size(43, 20);
            this.TextThreads.TabIndex = 25;
            this.TextThreads.Text = "100";
            // 
            // TextRepeatCount
            // 
            this.TextRepeatCount.Location = new System.Drawing.Point(207, 60);
            this.TextRepeatCount.Mask = "99999";
            this.TextRepeatCount.Name = "TextRepeatCount";
            this.TextRepeatCount.Size = new System.Drawing.Size(49, 20);
            this.TextRepeatCount.TabIndex = 26;
            this.TextRepeatCount.Text = "1";
            // 
            // CBforever
            // 
            this.CBforever.AutoSize = true;
            this.CBforever.Location = new System.Drawing.Point(123, 63);
            this.CBforever.Name = "CBforever";
            this.CBforever.Size = new System.Drawing.Size(59, 17);
            this.CBforever.TabIndex = 27;
            this.CBforever.Text = "forever";
            this.CBforever.UseVisualStyleBackColor = true;
            this.CBforever.CheckedChanged += new System.EventHandler(this.CBforever_CheckedChanged);
            // 
            // buttonReset
            // 
            this.buttonReset.Enabled = false;
            this.buttonReset.Location = new System.Drawing.Point(436, 111);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(73, 30);
            this.buttonReset.TabIndex = 28;
            this.buttonReset.Text = "Reset Stats";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // labelRequestLastSec
            // 
            this.labelRequestLastSec.AutoSize = true;
            this.labelRequestLastSec.Location = new System.Drawing.Point(15, 161);
            this.labelRequestLastSec.Name = "labelRequestLastSec";
            this.labelRequestLastSec.Size = new System.Drawing.Size(102, 13);
            this.labelRequestLastSec.TabIndex = 29;
            this.labelRequestLastSec.Text = "Requests in last sec";
            // 
            // labelRequestsLastSec
            // 
            this.labelRequestsLastSec.AutoSize = true;
            this.labelRequestsLastSec.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRequestsLastSec.Location = new System.Drawing.Point(119, 161);
            this.labelRequestsLastSec.Name = "labelRequestsLastSec";
            this.labelRequestsLastSec.Size = new System.Drawing.Size(19, 20);
            this.labelRequestsLastSec.TabIndex = 30;
            this.labelRequestsLastSec.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 249);
            this.Controls.Add(this.labelRequestsLastSec);
            this.Controls.Add(this.labelRequestLastSec);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.CBforever);
            this.Controls.Add(this.TextRepeatCount);
            this.Controls.Add(this.TextThreads);
            this.Controls.Add(this.labelCountFailures);
            this.Controls.Add(this.labelCount200);
            this.Controls.Add(this.labelFailures);
            this.Controls.Add(this.label200);
            this.Controls.Add(this.ButtonStop);
            this.Controls.Add(this.labelRequestsPerSec);
            this.Controls.Add(this.labelValueTime);
            this.Controls.Add(this.labelValueRequests);
            this.Controls.Add(this.labelStats);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelTheads);
            this.Controls.Add(this.labelRequests);
            this.Controls.Add(this.ButtonRun);
            this.Controls.Add(this.labelRepeat);
            this.Controls.Add(this.labelURL);
            this.Controls.Add(this.TextURL);
            this.Name = "Form1";
            this.Text = "URLSTRESS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextURL;
        private System.Windows.Forms.Label labelURL;
        private System.Windows.Forms.Label labelRepeat;
        private System.Windows.Forms.Button ButtonRun;
        private System.Windows.Forms.Label labelRequests;
        private System.Windows.Forms.Label labelTheads;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelStats;
        private System.Windows.Forms.Label labelValueRequests;
        private System.Windows.Forms.Label labelValueTime;
        private System.Windows.Forms.Label labelRequestsPerSec;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button ButtonStop;
        private System.Windows.Forms.Label label200;
        private System.Windows.Forms.Label labelFailures;
        private System.Windows.Forms.Label labelCount200;
        private System.Windows.Forms.Label labelCountFailures;
        private System.Windows.Forms.MaskedTextBox TextThreads;
        private System.Windows.Forms.MaskedTextBox TextRepeatCount;
        private System.Windows.Forms.CheckBox CBforever;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Label labelRequestLastSec;
        private System.Windows.Forms.Label labelRequestsLastSec;
    }
}

