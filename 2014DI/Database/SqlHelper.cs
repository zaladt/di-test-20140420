﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    class SqlHelper
    {
        public enum size
        {
            Max = 1
        }

        public static SqlParameter CreateSqlVarCharParameter(string name, string value)
        {
            SqlParameter param = new SqlParameter(name, SqlDbType.VarChar);
            if (value == null)
            {
                param.Value = DBNull.Value;
            }
            else
            {
                param.Value = value;
            }
            return param;
        }

        static public SqlParameter CreateSqlParameter(string name, SqlDbType type, object value)
        {
            if (type == SqlDbType.VarChar) //Should be a list of more types
                throw new Exception("You should use the overloaded method that accepts a size!");

            SqlParameter parm = new SqlParameter(name, type);
            parm.Value = value == null ? DBNull.Value : value;
            return parm;
        }

        static public SqlParameter CreateSqlParameter(string name, SqlDbType type, size size, object value)
        {
            if (type != SqlDbType.VarChar && type != SqlDbType.VarBinary)
                throw new Exception("Only supported for varchar(max), varbinary(max)");

            SqlParameter parm = new SqlParameter(name, type);
            parm.Value = value == null ? DBNull.Value : value;
            return parm;
        }

        static public SqlParameter CreateSqlParameter(string name, SqlDbType type, int size, object value)
        {
            SqlParameter parm = new SqlParameter(name, type, size);
            parm.Value = value == null ? DBNull.Value : value;
            return parm;
        }

    }
}
