﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Shared.DataTransferObjects;

namespace DataLayer
{
    public class BandAccess : BaseDataAccess
    {

        public List<BandDTO> GetBandsByPrefix(string prefix)
        {
            var result = new List<BandDTO>();
            
            var parameters = new List<SqlParameter>();

            parameters.Add(SqlHelper.CreateSqlParameter("@BandNamePrefix", SqlDbType.VarChar, 200, prefix));
            IDataReader reader = ExecuteStoreProcedure("GetBandsByPrefix", parameters);

            using (reader)
            {
                while (reader.Read())
                {
                    string bandName = reader["BandName"].ToString();
                    string origin = reader["Origin"].ToString();

                    result.Add(new BandDTO()
                        {
                            BandName = bandName,Origin = origin
                        });


                }
            }
            return result;
        }

    }
}
