﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace DataLayer
{
    abstract public class BaseDataAccess : IDisposable
    {
           /// <summary>
        /// The Connetionstring name that will be used as default
        /// </summary>
        private const string _DEFAULT_CONNECTION = "BandDB";

        private DbCommand _dbCommand;
        private Database _db;
        private DbConnection _connection;
        private DbTransaction _transaction = null;

        public BaseDataAccess()
        {
            ConnectDB(_DEFAULT_CONNECTION);
        }

        public BaseDataAccess(string connectionStringName)
        {
            ConnectDB(connectionStringName);
        }

        public void ConnectDB(string connectionStringName)
        {
            _db = GetDatabase(connectionStringName);

            
            _connection = _db.CreateConnection();
              
        }

        public Database GetDatabase(string Name)
        {
            return new SqlDatabase(ConfigurationManager.ConnectionStrings[Name].ConnectionString);
        }

        protected IDataReader ExecuteStoreProcedure(string storeProcName, List<SqlParameter> parameters)
        {
            IDataReader dataReader = null;
            _dbCommand = _db.GetStoredProcCommand(storeProcName);

            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }

            if (_transaction != null)
                dataReader = _db.ExecuteReader(_dbCommand, _transaction);
            else
                dataReader = _db.ExecuteReader(_dbCommand);

            return dataReader;
        }

        protected IDataReader ExecuteQuery(string SQLQuery, List<SqlParameter> parameters)
        {
            IDataReader reader;

            _dbCommand = _db.GetSqlStringCommand(SQLQuery);
            _dbCommand.CommandType = CommandType.Text;

            if (parameters != null)
            {
                _dbCommand.Parameters.AddRange(parameters.ToArray());
            }

            if (_transaction != null)
                reader = _db.ExecuteReader(_dbCommand, _transaction);
            else
                reader = _db.ExecuteReader(_dbCommand);

            return reader;
        }

        protected object ExecuteScalar(string SQLQuery, List<SqlParameter> parameters)
        {
            object result = null;

            _dbCommand = _db.GetSqlStringCommand(SQLQuery);


            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }

            if (_transaction != null)
                result = _db.ExecuteScalar(_dbCommand, _transaction);
            else
                result = _db.ExecuteScalar(_dbCommand);

            return result;
        }

        protected int ExecuteNonQuery(string SQLQuery, List<SqlParameter> parameters)
        {
            int returnValue = 0;
            _dbCommand = _db.GetSqlStringCommand(SQLQuery);

            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }

            if (_transaction != null)
                returnValue = _db.ExecuteNonQuery(_dbCommand, _transaction);
            else
                returnValue = _db.ExecuteNonQuery(_dbCommand);

            return returnValue;
        }

        protected int ExecuteStoreProcedureNonQuery(string StoreProcName, List<SqlParameter> Parameters)
        {
            int returnValue = 0;
            _dbCommand = _db.GetStoredProcCommand(StoreProcName);

            if (Parameters != null)
            {
                foreach (var parameter in Parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }
            if (_transaction != null)
                returnValue = _db.ExecuteNonQuery(_dbCommand, _transaction);
            else
                returnValue = _db.ExecuteNonQuery(_dbCommand);

            return returnValue;
        }

        protected int ExecuteStoreProcedureNonQuery(string StoreProcName, List<SqlParameter> Parameters, int commandTimeout)
        {
            int returnValue = 0;
            _dbCommand = _db.GetStoredProcCommand(StoreProcName);
            _dbCommand.CommandTimeout = commandTimeout;


            if (Parameters != null)
            {
                foreach (var parameter in Parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }
            if (_transaction != null)
                returnValue = _db.ExecuteNonQuery(_dbCommand, _transaction);
            else
                returnValue = _db.ExecuteNonQuery(_dbCommand);

            return returnValue;
        }

        protected object ExecuteStoreProcedureScalar(string StoreProcName, List<SqlParameter> Parameters)
        {
            object returnValue = null;
            _dbCommand = _db.GetStoredProcCommand(StoreProcName);

            if (Parameters != null)
            {
                foreach (var parameter in Parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }
            if (_transaction != null)
                returnValue = _db.ExecuteScalar(_dbCommand, _transaction);
            else
                returnValue = _db.ExecuteScalar(_dbCommand);

            return returnValue;
        }

        protected object ExecuteStoreProcedureScalar(string StoreProcName, List<SqlParameter> Parameters, int commandTimeout)
        {
            object returnValue = null;
            _dbCommand = _db.GetStoredProcCommand(StoreProcName);
            _dbCommand.CommandTimeout = commandTimeout;

            if (Parameters != null)
            {
                foreach (var parameter in Parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }
            if (_transaction != null)
                returnValue = _db.ExecuteScalar(_dbCommand, _transaction);
            else
                returnValue = _db.ExecuteScalar(_dbCommand);

            return returnValue;
        }

        protected int ExecuteStoredProcedureRetVal(string StoredProcName, List<SqlParameter> Parameters)
        {
            _dbCommand = _db.GetStoredProcCommand(StoredProcName);

            if (Parameters != null)
            {
                foreach (var parameter in Parameters)
                {
                    _dbCommand.Parameters.Add(parameter);
                }
            }

            // Add return parameter
            SqlParameter retParm = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
            retParm.Direction = ParameterDirection.ReturnValue;
            _dbCommand.Parameters.Add(retParm);


            if (_transaction != null)
                _db.ExecuteNonQuery(_dbCommand, _transaction);
            else
                _db.ExecuteNonQuery(_dbCommand);

            string retVal = _dbCommand.Parameters["@RETURN_VALUE"].Value.ToString();

            return Convert.ToInt32(retVal);
        }

      

        public string GetIdListAsCommaSeparatedString(List<int> idList)
        {
            string result = null;

            foreach (int id in idList)
            {
                result += string.Format("{0},", id);
            }

            if (result != null)
            {
                return result.TrimEnd(',');
            }
            return string.Empty;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_dbCommand.Connection != null)
            {
                _dbCommand.Connection.Close();
            }
        }

        #endregion

    
    }
}
