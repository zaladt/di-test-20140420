﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer;
using Shared.DataTransferObjects;

namespace BusinessLayer
{
    public class BandHandler
    {
        public List<BandDTO> GetBandsByPrefix(string prefix)
        {
            var bandAccess = new BandAccess();
            return bandAccess.GetBandsByPrefix(prefix);
        }

    }
}
