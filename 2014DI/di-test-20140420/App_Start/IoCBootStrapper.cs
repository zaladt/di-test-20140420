﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ClientFacade;
using ClientFacade.Fakes;
using ClientFacade.Interfaces;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;

namespace di_test_20140420.App_Start
{
    public class IoCBootStrapper
    {

        public static readonly Container Container = new Container();

        public static void CreateIoC()
        {
            RegisterImplementations(Container);

            Container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            Container.RegisterMvcAttributeFilterProvider();

            Container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(Container));

        }

        private static void RegisterImplementations(Container container)
        {
            bool fakesActive = false;
            if (!Boolean.TryParse(ConfigurationManager.AppSettings["FakesActive"], out fakesActive))
            {
                RegisterRealImplementations(container);
                return;
            }

            if (fakesActive)
            {
                RegisterFakeImplementations(container);
            }
            else
            {
                RegisterRealImplementations(container);
            }
           

        }

        private static void RegisterRealImplementations(Container container)
        {
            container.RegisterSingle<IBandFacade, BandFacade>();
            container.RegisterSingle<IBandEventFacade, BandEventFacade>();
        }

        private static void RegisterFakeImplementations(Container container)
        {
            container.RegisterSingle<IBandFacade, BandFakeFacade>();
            container.RegisterSingle<IBandEventFacade, BandEventFakeFacade>();
        }
    }
}