﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace di_test_20140420.ViewModels
{
    public class BandVM
    {

        public string SearchText { get; set; }

        public string BandsAsJson { get; set; }
    }
}