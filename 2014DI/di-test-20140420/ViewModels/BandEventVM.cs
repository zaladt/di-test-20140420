﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace di_test_20140420.ViewModels
{
    public class BandEventVM
    {
        public string SearchText { get; set; }

        public string EventsAsJson { get; set; }

    }
}