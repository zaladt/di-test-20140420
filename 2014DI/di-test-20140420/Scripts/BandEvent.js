﻿var homeConfig = {

    JsonEvents: "#EventsAsJson",
    EventTable: "#eventTable"

};

function InitHome() {


    $(document).ready(function () {
        PopulateBandTable();
    });

}

function PopulateBandTable() {
    var bandsSerialized = $(homeConfig.JsonEvents).val();

    if (bandsSerialized !== undefined &&
        bandsSerialized != null &&
        bandsSerialized != "") {

        var bandDeserialized = JSON.parse(bandsSerialized);

        $.each(bandDeserialized, function (i, band) {
            $(homeConfig.EventTable).find('tbody')
            .append($('<tr>')
                .append($('<td data-th="HeadLiner">')
                    .append($('<p>')
                        .text(band.HeadLiner)
                    )
                )
                .append($('<td data-th="City">')
                    .append($('<p>')
                        .text(band.City)
                    )
                )
                .append($('<td data-th="Date">')
                    .append($('<p>')
                        .text(band.Date)
                    )
                )
                .append($('<td data-th="Venue">')
                    .append($('<p>')
                        .text(band.Venue)
                    )
                )
               
                .append($('<td data-th="OtherArtists">')
                    .append($('<p>')
                        .text(band.OtherArtists)
                    )
                )

                .append($('<td data-th="Link">')
                    .append($('<a>')
                        .text("Link")
                        .attr("href",band.Url)
                    )
                )
            
            );
        });
    }
}