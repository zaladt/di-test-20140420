﻿var homeConfig = {
  
    JsonBands: "#BandsAsJson",
    BandTable : "#bandTable"
  
};

function InitHome() {
   
    
    $(document).ready(function () {
        PopulateBandTable();
    });

}

function PopulateBandTable() {
    var bandsSerialized = $(homeConfig.JsonBands).val();

    if (bandsSerialized !== undefined &&
        bandsSerialized != null &&
        bandsSerialized != "") {

        var bandDeserialized = JSON.parse(bandsSerialized);

        $.each(bandDeserialized, function(i, band) {
            $(homeConfig.BandTable).find('tbody')
            .append($('<tr>')
                .append($('<td data-th="Band">')
                    .append($('<p>')
                        .text(band.BandName)
                    )
                )
                .append($('<td data-th="Origin">')
                    .append($('<p>')
                        .text(band.Origin)
                    )
                )
            );
        });
    }
}