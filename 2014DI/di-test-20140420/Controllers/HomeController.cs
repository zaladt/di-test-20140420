﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientFacade.Interfaces;
using di_test_20140420.ViewModelBuilders;
using di_test_20140420.ViewModels;

namespace di_test_20140420.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBandFacade _bandFacade;

        public HomeController(IBandFacade _injectedBandFacade)
        {
            _bandFacade = _injectedBandFacade;

        }
        
        public ActionResult Index()
        {
            BandVM model = BandVMBuilder.BuildModel(_bandFacade.GetBandsByPrefix("Wata"));
            return View(model);
        }
        
        [HttpPost]
        public ActionResult GetBandByPrefix(BandVM bandVm)
        {
            if (!string.IsNullOrEmpty(bandVm.SearchText))
            {
                bandVm = BandVMBuilder.BuildModel(_bandFacade.GetBandsByPrefix(bandVm.SearchText));
            }
            return View("Index", bandVm);
        }

        [HttpPost]
        public ActionResult GetBandByPrefixNoCheck(BandVM bandVm)
        {
           bandVm = BandVMBuilder.BuildModel(_bandFacade.GetBandsByPrefix(bandVm.SearchText));
           return View("Index", bandVm);
        }

    }
}
