﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClientFacade.Interfaces;
using di_test_20140420.Parsers;
using di_test_20140420.ViewModelBuilders;
using di_test_20140420.ViewModels;

namespace di_test_20140420.Controllers
{
    public class BandEventController : Controller
    {
       
        private readonly IBandEventFacade _bandEventFacade;

        public BandEventController(IBandEventFacade injectedBandEventFacade)
        {
            _bandEventFacade = injectedBandEventFacade;
        }

        public ActionResult Index()
        {
            return View(new BandEventVM());
        }

        public ActionResult SearchForBands(BandEventVM model)
        {
            model =  SearchForEventsByBandName(model.SearchText);
            return View("Index", model);
        }


        private BandEventVM SearchForEventsByBandName(string searchText)
        {
            return BandEventVMBuilder.BuildModel(_bandEventFacade.GetEventsFromLastFm(searchText));
        }

        
    }
}
