﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using di_test_20140420.Parsers;
using di_test_20140420.ViewModels;

namespace di_test_20140420.ViewModelBuilders
{
    public class BandEventVMBuilder
    {
        public static BandEventVM BuildModel(List<GigInfoClient> infos)
        {
            var model = new BandEventVM();
            var serializer = new JavaScriptSerializer();
            model.EventsAsJson = serializer.Serialize(infos);
            return model;

        }

    }
}