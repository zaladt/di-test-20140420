﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Shared.DataTransferObjects;
using di_test_20140420.ViewModels;

namespace di_test_20140420.ViewModelBuilders
{
    public class BandVMBuilder
    {
        public static BandVM BuildModel(List<BandDTO> bands)
        {
            var model = new BandVM();
            var serializer = new JavaScriptSerializer();
            model.BandsAsJson = serializer.Serialize(bands);
            return model;
        }

    }
}