﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ClientFacade.Interfaces;
using Shared.Parsers;
using di_test_20140420.Parsers;

namespace ClientFacade
{
    public class BandEventFacade : IBandEventFacade
    {
        public List<GigInfoClient> GetEventsFromLastFm(string searchText)
        {
            var request = (HttpWebRequest)WebRequest.Create(LastFmGetEventUri.Replace("{0}", searchText));
            request.Method = "POST";
            var response = request.GetResponse();
            return GetLastFmResponse(response);

        }

        private List<GigInfoClient> GetLastFmResponse(WebResponse response)
        {
            var result = new List<GigInfoClient>();
            try
            {
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();
                result = LastFm.GetEventsFromJson(responseString);

                if (streamResponse != null) streamResponse.Close();
                streamRead.Close();
                response.Close();
            }
            catch 
            {
                return new List<GigInfoClient>();
            }
            return result;
        }


        #region 
        // Om ni vill gå mot Last.FM måste ni tyvärr skapa ett eget konto, väldigt simpel registrering och byt ut länken nedan mot eran nyckel
        public string LastFmGetEventUri =
         "http://ws.audioscrobbler.com/2.0/?method=artist.getevents&artist={0}&api_key=http://www.last.fm/api/account/create&format=json";
        #endregion

    }
}
