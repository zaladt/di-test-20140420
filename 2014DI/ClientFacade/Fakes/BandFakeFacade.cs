﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientFacade.Interfaces;
using Shared.DataTransferObjects;

namespace ClientFacade.Fakes
{
    public class BandFakeFacade : IBandFacade 
    {
        public List<BandDTO> GetBandsByPrefix(string prefix)
        {
            var result = new List<BandDTO>();

            result.Add(new BandDTO()
            {
                BandName = "Watain",
                Origin = "Stockholm, Sweden"

            });

            if (!result.Any(x => x.BandName.Contains(prefix)))
            {
                return new List<BandDTO>();
            }
            else
            {
                return result.Where(X => X.BandName.Contains(prefix)).ToList();

            }
        }
    }
}
