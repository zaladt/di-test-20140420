﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientFacade.Interfaces;
using di_test_20140420.Parsers;

namespace ClientFacade.Fakes
{
    public class BandEventFakeFacade : IBandEventFacade  
    {
        public List<GigInfoClient> GetEventsFromLastFm(string searchText)
        {
            return new List<GigInfoClient>()
                {
                    new GigInfoClient("2014-10-19")
                        {
                            City = "Stockholm",
                            HeadLiner = "Watain",
                            HeadLinerSpotifyLink = "http://www.spotify.com",
                            OtherArtists = "None",
                            Url = "http://last.fm",
                            Venue = "Globen"

                        }
                };
        }
    }
}
