﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;
using ClientFacade.Interfaces;
using Shared.DataTransferObjects;

namespace ClientFacade
{
    public class BandFacade : IBandFacade
    {
        public List<BandDTO> GetBandsByPrefix(string prefix)
        {
            var bandHandler = new BandHandler();
            return bandHandler.GetBandsByPrefix(prefix);
        }
    }
}
