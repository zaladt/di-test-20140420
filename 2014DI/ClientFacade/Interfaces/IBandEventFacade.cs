﻿using System.Collections.Generic;
using System.Net;
using di_test_20140420.Parsers;

namespace ClientFacade.Interfaces
{
    public interface IBandEventFacade
    {
        List<GigInfoClient> GetEventsFromLastFm(string searchText);
        
    }
}