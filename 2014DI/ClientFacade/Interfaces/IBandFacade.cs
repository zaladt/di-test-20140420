using System.Collections.Generic;
using Shared.DataTransferObjects;

namespace ClientFacade.Interfaces
{
    public interface IBandFacade
    {
        List<BandDTO> GetBandsByPrefix(string prefix);
    }
}