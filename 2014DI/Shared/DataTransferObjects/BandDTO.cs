﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.DataTransferObjects
{
    public class BandDTO
    {
        public string BandName { get; set; }

        public string Origin { get; set; }

    }
}
