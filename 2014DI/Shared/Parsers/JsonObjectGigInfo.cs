﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace di_test_20140420.Parsers
{
    public class JsonObjectGigInfo
    {
        public Events events { get; set; }
    }

   

    public class Events
    {
        [JsonProperty(PropertyName = "event")]
        [JsonConverter(typeof(MyEventConverter))]
        public List<Event> _event { get; set; }
        [JsonProperty(PropertyName = "@attr")]
        public Attr attr { get; set; }
    }


    public class Attr
    {
        public string artist { get; set; }
        public string festivalsonly { get; set; }
        public string page { get; set; }
        public string perPage { get; set; }
        public string totalPages { get; set; }
        public string total { get; set; }
    }

    public class Event
    {
        public string title { get; set; }
        public Artists artists { get; set; }
        public string headliner { get; set; }
        public Venue venue { get; set; }
        public string startDate { get; set; }
        public string description { get; set; }
        //public List<Image1> image { get; set; }
        public string attendance { get; set; }
        public string reviews { get; set; }
        public string tag { get; set; }
        public string url { get; set; }
        public string website { get; set; }
        public string tickets { get; set; }
        public string cancelled { get; set; }
    }


    public class MyEventConverter : JsonConverter
    {

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            try
            {

                if (reader.TokenType == JsonToken.StartArray)
                {
                    return serializer.Deserialize<List<Event>>(reader);
                }
                else
                {
                    List<Event> single = new List<Event>();
                    single.Add(serializer.Deserialize<Event>(reader));
                    return single;
                }
            }
            catch (Exception)
            {
                return new Artists();
            }


        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

    }

    public class MyArtistConverter : JsonConverter
    {

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            try
            {

                if (reader.TokenType == JsonToken.StartArray)
                {
                    return serializer.Deserialize<List<string>>(reader);
                }
                else
                {
                    List<string> single = new List<string>();
                    single.Add(serializer.Deserialize<string>(reader));
                    return single;
                }
            }
            catch (Exception)
            {
                return new Artists();
            }


        }

        public override bool CanConvert(Type objectType)
        {
            return true;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }

    }

    public class Artists
    {
        [JsonConverter(typeof(MyArtistConverter))]
        public List<string> artist { get; set; }
        public string headliner { get; set; }
    }

    public class Venue
    {
        public string id { get; set; }
        public string name { get; set; }
        public Location location { get; set; }
        public string url { get; set; }
        public string website { get; set; }
        public string phonenumber { get; set; }
        public List<Image> image { get; set; }
    }

    public class Location
    {
        public GeoPoint geopoint { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string street { get; set; }
        public string postalcode { get; set; }
    }

    public class GeoPoint
    {
        public string geolat { get; set; }
        public string geolong { get; set; }
    }

    public class Image
    {
        public string text { get; set; }
        public string size { get; set; }
    }

    public class Image1
    {
        public string text { get; set; }
        public string size { get; set; }
    }
}