﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace di_test_20140420.Parsers
{
    public class GigInfoClient
    {


        public string Date { get; set; }
        public string Url { get; set; }
        public string Venue { get; set; }
        public string City { get; set; }
        public string HeadLiner { get; set; }
        public string HeadLinerSpotifyLink { get; set; }
        public string OtherArtists { get; set; }

        public GigInfoClient(string startDate, string url, string venue, Location location, Artists artists)
        {
            DateTime parsedDate = DateTime.Now;
            DateTime.TryParse(startDate, out parsedDate);
            Date = parsedDate.ToShortDateString();
            Url = url;
            Venue = venue;
            City = location.city;
            if (artists != null && !string.IsNullOrEmpty(artists.headliner) && artists.artist != null && artists.artist.Any())
            {
                HeadLiner = artists.headliner;
                OtherArtists = string.Join(",", artists.artist.ToArray().Where(x => x.ToLower() != HeadLiner.ToLower()));
            }

            if (Cache.ArtistCache != null && Cache.ArtistCache.ContainsKey(HeadLiner))
            {
                HeadLinerSpotifyLink = Cache.ArtistCache[HeadLiner];
            }

        }



        public GigInfoClient(string date)
        {
            Date = date;
        }


        public static class Cache
        {

            public static Dictionary<string, string> ArtistCache { get; set; }

            public static void AddToArtistCache(string arttistName, string url)
            {
                if (ArtistCache == null)
                {
                    ArtistCache = new Dictionary<string, string>();
                }

                if (!ArtistCache.ContainsKey(arttistName))
                {
                    ArtistCache.Add(arttistName, url);
                }
            }

        }

    }
}