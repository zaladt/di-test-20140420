﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json; 
using di_test_20140420.Parsers;

namespace Shared.Parsers
{

    public static class LastFm
    {

        public static List<GigInfoClient> GetEventsFromJson(string responseString)
        {
            var events = JsonConvert.DeserializeObject<JsonObjectGigInfo>(responseString);
            return GetGigInfoFromJsonObject(events);
           
        }

        public static List<GigInfoClient> GetGigInfoFromJsonObject(JsonObjectGigInfo jsonObjectGigInfo)
        {
            var result = new List<GigInfoClient>();

            if (jsonObjectGigInfo == null || jsonObjectGigInfo.events == null || jsonObjectGigInfo.events._event == null)
            {
                result.Add(new GigInfoClient("No gigs found :("));
                return result;
            }

            foreach (var row in jsonObjectGigInfo.events._event.Where(x => x.venue != null))
            {
                result.Add(new GigInfoClient(row.startDate, row.url, row.venue.name, row.venue.location, row.artists));
            }


            if (!result.Any())
            {
                result.Add(new GigInfoClient("No gigs found :("));
            }
            return result;
        }

    }
}