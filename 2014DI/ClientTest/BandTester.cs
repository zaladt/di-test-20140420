﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ClientFacade.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Shared.DataTransferObjects;
using di_test_20140420.Controllers;
using di_test_20140420.ViewModels;



namespace ClientTest
{
    [TestClass]
    public class BandTester
    {
       
        private HomeController homeController;

        [TestInitialize]
        public void Init()
        {
            
        }


        [TestMethod]
        [ExpectedException(typeof(NullReferenceException), "Object reference not set to an instance of an object")]
        public void CallingGetBandWithoutPrefixShouldThrowException()
        {
            homeController = new HomeController(null);
            homeController.GetBandByPrefixNoCheck(new BandVM());

            Assert.Fail("Test did not throw exception");
        }


        [TestMethod]
        public void GetBandShouldReturnOriginAndBandName()
        {
            string searchText = "Watain";
            var postModel = new BandVM()
                {
                    SearchText = searchText
                };

            var mBandFacade = new Mock<IBandFacade>();

            homeController = SetupMocks(searchText,mBandFacade);
            var resultAsViewResult = (ViewResult)homeController.GetBandByPrefix(postModel);

            var model = resultAsViewResult.Model as BandVM;
            if (model != null)
            {
                var deserializedListOfBands = JsonConvert.DeserializeObject<List<BandDTO>>(model.BandsAsJson).ToList();
                Assert.IsTrue(deserializedListOfBands.Any(X=> X.BandName.Contains(searchText)));
                mBandFacade.Verify(X=> X.GetBandsByPrefix(searchText), Times.Exactly(2));
             
            }
            else
            {
                Assert.Fail("No model was returned to view");
            }
        }

        private HomeController SetupMocks(string searchText, Mock<IBandFacade> mBandFacade)
        {
            
            mBandFacade.Setup(X => X.GetBandsByPrefix(searchText)).Returns(new List<BandDTO>()
                {
                    new BandDTO()
                        {
                            BandName = searchText,
                            Origin = "Stockholm"
                        }
                });

            var bandFacade = mBandFacade.Object;

            return new HomeController(bandFacade);

        }


    }
}
